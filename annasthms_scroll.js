/********************************
 * script by annasthms          *
 * may 4, 2020                  *
 *******************************/
$(document).ready(function () {
  const container = $("#maain"), // the container of the content sections
    sections = $(".one-question"), // the content sections
    nav = $(".hash-links"), // the container with only the jump links
    topbar = $("#topbar"), // topbar
    scrollOffset = parseInt(getComputedStyle(document.documentElement)
                  .getPropertyValue("--Question-Spacing")) - 1,
                  // number of px to offset the top of the scrolled to section by
    scrollSpeed = getComputedStyle(document.documentElement)
                  .getPropertyValue("--Jump-Scroll-Speed");

  const topbarHeight = topbar.outerHeight();

  $("#cont-a").on("scroll", function () {
    let cur_pos = $(this).scrollTop();
    let screenHeight = $(this).outerHeight();

    nav.find("a").removeClass("current");
    sections.removeClass("current");

    sections.each(function () {
      let top = $(this).offset().top - topbarHeight + 50,
        bottom = top + $(this).outerHeight() - 50;

      if (
        (top > 0 &&
          top <= screenHeight) ||
        (bottom < screenHeight && bottom > 0)
      ) {
        $(this).addClass("current");
        nav
          .find('a[href="#' + $(this).attr("id") + '"]')
          .addClass("current");
      } else if (top < 0 && bottom > screenHeight) {
        $(this).addClass("current");
        nav
          .find('a[href="#' + $(this).attr("id") + '"]')
          .addClass("current");
      } else {
        $(this).removeClass("current");
        nav
          .find('a[href="#' + $(this).attr("id") + '"]')
          .removeClass("current");
      }
    });
  });

  nav.find("a").on("click", function () {
    var $el = $(this),
      id = $el.attr("href");

    sectionPosition = $(id).offset().top;
    scrollTo = sectionPosition - scrollOffset - topbarHeight + $("#cont-a").scrollTop() + "px";

    $("#cont-a").animate({
        scrollTop: scrollTo,
      },
      scrollSpeed
    );

    return false;
  });
}); //end ready
